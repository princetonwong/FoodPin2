//
//  Restaurant.swift
//  FoodPin
//
//  Created by Princeton Wong on 3/1/2017.
//  Copyright © 2017 PK's Idée. All rights reserved.
//

import Foundation

class Restaurant {
    var name = ""
    var type = ""
    var location = ""
    var image = ""
    var isVisited = true
    var phone = ""
    var rating = ""
    
    
    init(name: String, type: String, location: String, phone: String, image: String,  isVisited: Bool) {
        self.name = name
        self.type = type
        self.location = location
        self.image = image
        self.isVisited = isVisited
        self.phone = phone
        
    }
}
