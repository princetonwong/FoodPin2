//
//  RestaurantDetailTableViewCell.swift
//  FoodPin
//
//  Created by Princeton Wong on 4/1/2017.
//  Copyright © 2017 PK's Idée. All rights reserved.
//

import UIKit

class RestaurantDetailTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBOutlet weak var fieldLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
}
